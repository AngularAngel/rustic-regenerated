/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.rusticregen.common.blocks;

import net.angle.rusticregen.common.blocks.entities.LeafCoveredBlockEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

/**
 *
 * @author angle
 */
public class LeafCoveredBlock extends LeavesBlock implements SimpleWaterloggedBlock, LeafCoveredEntityBlock {
   public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
    
    public LeafCoveredBlock() {
        this(Properties.copy(Blocks.OAK_LEAVES).dynamicShape());
    }
    
    public LeafCoveredBlock(Properties properties) {
        super(properties);
        this.registerDefaultState(this.defaultBlockState().setValue(PERSISTENT, true).setValue(WATERLOGGED, false));
    }
    
    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(WATERLOGGED);
    }
    
    @Override
    public LeafCoveredBlockEntity getBlockEntity(BlockGetter level, BlockPos pos) {
        BlockEntity blockEntity = level.getBlockEntity(pos);
        if (blockEntity instanceof LeafCoveredBlockEntity)
            return (LeafCoveredBlockEntity) level.getBlockEntity(pos);
        else return null;
    }
    
    @Override
    public LeafCoveredBlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return new LeafCoveredBlockEntity(pos, state);
    }

    @Override
    public BlockState updateShape(BlockState state, Direction direction, BlockState neighbourState, LevelAccessor level, BlockPos pos, BlockPos neighborPos) {
        getBlockEntity(level, pos).updateInternalBlockState(direction, neighbourState, level, pos, neighborPos);
        return super.updateShape(state, direction, state, level, pos, pos);
    }

    @Override
    public void onRemove(BlockState oldState, Level level, BlockPos pos, BlockState newState, boolean bool) {
        BlockState internalBlockState = getBlockEntity(level, pos).getInternalBlockState();
        if (newState != internalBlockState && newState.getBlock() != this)
            popResource(level, pos, new ItemStack(internalBlockState.getBlock().asItem()));
        super.onRemove(oldState, level, pos, newState, bool); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float getDestroyProgress(BlockState state, Player player, BlockGetter getter, BlockPos pos) {
        BlockState internalBlockState = getBlockEntity(getter, pos).getInternalBlockState();
        return internalBlockState.getBlock().getDestroyProgress(internalBlockState, player, getter, pos);
    }

    @Override
    public SoundType getSoundType(BlockState state, LevelReader world, BlockPos pos, Entity entity) {
        BlockState internalBlockState = getBlockEntity(world, pos).getInternalBlockState();
        return internalBlockState.getBlock().getSoundType(internalBlockState, world, pos, entity);
    }

    @Override
    public VoxelShape getBlockSupportShape(BlockState state, BlockGetter getter, BlockPos pos) {
        LeafCoveredBlockEntity blockEntity = getBlockEntity(getter, pos);
        if (blockEntity == null)
            return Shapes.block();
        else {
            BlockState internalBlockState = blockEntity.getInternalBlockState();
            if (internalBlockState == null)
                return Shapes.block();
            else
                return internalBlockState.getBlock().getBlockSupportShape(internalBlockState, getter, pos);
        }
    }

    @Override
    public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand hand, BlockHitResult result) {
        LeafCoveredBlockEntity blockEntity = getBlockEntity(level, pos);
        ItemStack itemInHand = player.getItemInHand(hand);
        if (isShears(itemInHand.getItem())) {
            popResource(level, pos, new ItemStack(blockEntity.getLeafState().getBlock().asItem()));
            level.setBlock(pos, blockEntity.getInternalBlockState(), UPDATE_ALL);
             if (!player.isCreative())
                itemInHand.hurtAndBreak(1, player, (p_150686_) -> {
                   p_150686_.broadcastBreakEvent(hand);
                });
            return InteractionResult.SUCCESS;
        }
        return InteractionResult.FAIL;
    }

    @Override
    public int getLightEmission(BlockState state, BlockGetter getter, BlockPos pos) {
        LeafCoveredBlockEntity blockEntity = getBlockEntity(getter, pos);
        if (blockEntity == null)
            return super.getLightEmission(state, getter, pos);
        else {
            BlockState internalBlockState = blockEntity.getInternalBlockState();
            if (internalBlockState == null)
                return super.getLightEmission(state, getter, pos);
            else
                return internalBlockState.getBlock().getLightEmission(internalBlockState, getter, pos) - 3;
        }
    }

    @Override
    public FluidState getFluidState(BlockState state) {
        return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state);
    }
    
    @Override
    public boolean canPlaceLiquid(BlockGetter getter, BlockPos pos, BlockState state, Fluid fluid) {
        return !state.getValue(WATERLOGGED) && fluid == Fluids.WATER;
    }
    
    @Override
    public boolean placeLiquid(LevelAccessor level, BlockPos pos, BlockState state, FluidState fluid) {
        if (!state.getValue(WATERLOGGED) && fluid.getType() == Fluids.WATER) {
            if (!level.isClientSide()) {
                level.setBlock(pos, state.setValue(WATERLOGGED, true), 3);
                level.getLiquidTicks().scheduleTick(pos, fluid.getType(), fluid.getType().getTickDelay(level));
            }
            return true;
        } else
            return false;
    }
    
}
