/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.rusticregen.common.blocks;

import java.util.Random;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import static net.minecraft.world.level.block.Block.dropResources;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LeavesBlock;
import static net.minecraft.world.level.block.LeavesBlock.PERSISTENT;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;

/**
 *
 * @author angle
 */
public class GreatOakLeavesBlock extends LeavesBlock {
    
   public static final int DECAY_DISTANCE = 12;
   public static final IntegerProperty GREAT_OAK_DISTANCE = IntegerProperty.create("great_oak_distance", 1, DECAY_DISTANCE);
    
    public GreatOakLeavesBlock() {
        super(Properties.copy(Blocks.OAK_LEAVES));
        this.registerDefaultState(this.defaultBlockState().setValue(GREAT_OAK_DISTANCE, 1));
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(GREAT_OAK_DISTANCE);
    }
    
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext state) {
        return updateDistance(this.defaultBlockState().setValue(PERSISTENT, true), state.getLevel(), state.getClickedPos());
    }
    
    @Override
    public boolean isRandomlyTicking(BlockState state) {
        return state.getValue(GREAT_OAK_DISTANCE) == DECAY_DISTANCE && !state.getValue(PERSISTENT);
    }

    @Override
    public void tick(BlockState state, ServerLevel level, BlockPos pos, Random random) {
        level.setBlock(pos, updateDistance(state, level, pos), 3);
    }

    @Override
    public BlockState updateShape(BlockState state, Direction direction, BlockState neighbourState, LevelAccessor level, BlockPos pos, BlockPos neighbourPos) {
        int i = getDistanceAt(neighbourState) + 1;
        if (i != 1 || state.getValue(GREAT_OAK_DISTANCE) != i) {
            level.getBlockTicks().scheduleTick(pos, this, 1);
        }

        return state;
    }

   @Override
    public void randomTick(BlockState state, ServerLevel level, BlockPos pos, Random random) {
        if (!state.getValue(PERSISTENT) && state.getValue(GREAT_OAK_DISTANCE) == DECAY_DISTANCE) {
            dropResources(state, level, pos);
            level.removeBlock(pos, false);
        }

    }

   private static int getDistanceAt(BlockState state) {
        if (state.is(BlockTags.LOGS)) {
            return 0;
        } else {
            Block block = state.getBlock();
            return block instanceof GreatOakLeavesBlock ? state.getValue(GREAT_OAK_DISTANCE) : block instanceof LeavesBlock ? state.getValue(DISTANCE) : DECAY_DISTANCE;
        }
   }
    
    private static BlockState updateDistance(BlockState state, LevelAccessor level, BlockPos pos) {
      int i = DECAY_DISTANCE;
      BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();

      for(Direction direction : Direction.values()) {
         blockpos$mutableblockpos.setWithOffset(pos, direction);
         i = Math.min(i, getDistanceAt(level.getBlockState(blockpos$mutableblockpos)) + 1);
         if (i == 1) {
            break;
         }
      }

      return state.setValue(GREAT_OAK_DISTANCE, i);
   }
}
